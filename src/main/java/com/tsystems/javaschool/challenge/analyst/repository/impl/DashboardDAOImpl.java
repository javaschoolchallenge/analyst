package com.tsystems.javaschool.challenge.analyst.repository.impl;

import com.tsystems.javaschool.challenge.analyst.domain.Story;
import com.tsystems.javaschool.challenge.analyst.exception.DashboardNotAvailable;
import com.tsystems.javaschool.challenge.analyst.repository.DashboardDAO;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
@Slf4j
public class DashboardDAOImpl implements DashboardDAO {

    @Value("${web.dashboard.url}")
    private String dashboardUrl;

    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    public Story save(Story story) {
        log.info("Saving a story: {}", story);
        ResponseEntity<Story> response = restTemplate.postForEntity(dashboardUrl + "/story", story, Story.class);
        if (response.getStatusCode() != HttpStatus.CREATED) {
            throw new DashboardNotAvailable();
        }
        log.info("Story has been saved with ID {}", story.getId());
        return response.getBody();
    }

    @Override
    public Long getLastAnalystID() {
        log.info("Fetching free analysts");
        ResponseEntity<Long> response = restTemplate.getForEntity(dashboardUrl + "/analyst", Long.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new DashboardNotAvailable();
        }
        return response.getBody();
    }
}
