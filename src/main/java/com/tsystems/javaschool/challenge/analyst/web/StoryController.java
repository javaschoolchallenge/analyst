package com.tsystems.javaschool.challenge.analyst.web;

import com.tsystems.javaschool.challenge.analyst.domain.Story;
import com.tsystems.javaschool.challenge.analyst.service.WorkService;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("story")
@CrossOrigin
@RequiredArgsConstructor
public class StoryController {

    private final WorkService workService;

    @PostMapping
    public ResponseEntity<Story> createStory() {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(workService.createStory());
    }

}
