package com.tsystems.javaschool.challenge.analyst.repository;

import com.tsystems.javaschool.challenge.analyst.domain.Story;

public interface DashboardDAO {

    Story save(Story story);
    Long getLastAnalystID();
}
