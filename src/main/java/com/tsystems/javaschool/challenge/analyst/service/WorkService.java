package com.tsystems.javaschool.challenge.analyst.service;

import com.tsystems.javaschool.challenge.analyst.domain.Story;

public interface WorkService {

    Story createStory();

}
