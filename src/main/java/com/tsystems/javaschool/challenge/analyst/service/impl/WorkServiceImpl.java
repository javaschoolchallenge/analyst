package com.tsystems.javaschool.challenge.analyst.service.impl;

import com.tsystems.javaschool.challenge.analyst.domain.Employee;
import com.tsystems.javaschool.challenge.analyst.domain.Story;
import com.tsystems.javaschool.challenge.analyst.repository.DashboardDAO;
import com.tsystems.javaschool.challenge.analyst.service.WorkService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Random;

@Service
@RequiredArgsConstructor
@Slf4j
public class WorkServiceImpl implements WorkService {

    @Value("${business.allowed-estimations}")
    private int[] allowedEstimations;

    @Value("${business.allowed-priorities}")
    private int[] allowedPriorities;

    private final DashboardDAO dashboardDAO;

    private Random random = new Random();

    private Employee analyst;

    @PostConstruct
    public void initBot() {
        try {
            Long id = dashboardDAO.getLastAnalystID();
            analyst = new Employee("Analyst" + id, Employee.Position.ANALYST, true);
        } catch (Exception e) {
            analyst = new Employee("Analyst" + 1L, Employee.Position.ANALYST, true);
        }
        log.info("Analyst {} was initialized.", analyst.getName());
    }

    @Override
    public Story createStory() {
        var story = new Story();
        story.setEstimation(estimate());
        story.setPriority(priority());
        story.setStatus(Story.Status.OPEN);
        story.setAssigned(analyst);
        return dashboardDAO.save(story);
    }

    private int estimate() {
        return allowedEstimations[random.nextInt(allowedEstimations.length)];
    }

    private int priority() {
        return allowedPriorities[random.nextInt(allowedPriorities.length)];
    }
}
