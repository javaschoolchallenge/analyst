package com.tsystems.javaschool.challenge.analyst.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

    private String name;
    private Position position;
    private boolean isFreeStatus;

    public enum  Position {
        TESTER, DEVELOPER, ANALYST
    }
}
